using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Events;

public class FastMonoBehaviour : MonoBehaviour {

    protected GameManager gm => GameManager.Instance;

    public Vector3 pos { get { return transform.position; } set { transform.position = value; } }
    public Quaternion rot { get { return transform.rotation; } set { transform.rotation = value; } }
    public Vector3 scale { get { return transform.localScale; } set { transform.localScale = value; } }
}