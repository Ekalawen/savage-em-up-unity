using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CameraManager : FastMonoBehaviour {

    public Vector3 offset = new Vector3(0, 11, -7);
    public float speedCoef = 0.3f;
    public new Camera camera;

    public void Initialize() {
        camera.transform.position = TargetPos();
        camera.transform.LookAt(TargetPos() - offset);
    }

    public Vector3 TargetPos() {
        return gm.squadManager.GetPossessedCharacter().pos + offset;
    }

    public void Update() {
        UpdatePos();
    }

    protected void UpdatePos() {
        Vector3 targetPos = TargetPos();
        Vector3 currentPos = camera.transform.position;
        float magnitude = (targetPos - currentPos).magnitude;
        Vector3 direction = (targetPos - currentPos).normalized;
        float speed = magnitude * speedCoef;
        Vector3 movement = direction * speed * Time.deltaTime;
        movement = Vector3.ClampMagnitude(movement, magnitude);
        camera.transform.position += movement;
        //camera.transform.LookAt(TargetPos() - offset);
    }
}
