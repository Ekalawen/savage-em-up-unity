using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Bullet : FastMonoBehaviour {

    public bool destroyOnEnnemyHit = true;

    protected Damage damage;
    protected float maxRange = 10;
    protected float speed = 20;
    [HideInInspector]
    public Faction faction;
    [HideInInspector]
    public Vector3 direction;
    protected float duration;
    protected bool hasTriggered = false;
    protected List<Character> hittedEnnemies;

    [HideInInspector]
    public UnityEvent<Bullet> onHit = new UnityEvent<Bullet>();

    public void Initialize(Damage damage, float maxRange, float speed, Faction faction, Vector3 direction) {
        this.damage = damage;
        this.maxRange = maxRange;
        this.speed = speed;
        this.faction = faction;
        this.direction = direction;
        this.duration = maxRange / speed;
        gm.bulletManager.onCreateBullet.Invoke(this);
        hittedEnnemies = new List<Character>();
        Destroy(gameObject, duration);
    }

    protected void Update() {
        transform.position += direction * speed * Time.deltaTime;
    }

    protected void OnTriggerEnter(Collider other) {
        if(hasTriggered) {
            return;
        }
        Bullet bullet = other.GetComponentInParent<Bullet>();
        if(bullet != null) {
            return;
        }
        Character character = other.GetComponentInParent<Character>();
        if (character == null) {
            Destroy(gameObject);
            return;
        }
        if(character.faction.type == faction.type) {
            return;
        }
        if (hittedEnnemies.Contains(character)) {
            return;
        }
        character.hitable.HitBy(damage);
        onHit.Invoke(this);
        if (destroyOnEnnemyHit) {
            hasTriggered = true;
            Destroy(gameObject);
        }
        hittedEnnemies.Add(character);
    }
}
