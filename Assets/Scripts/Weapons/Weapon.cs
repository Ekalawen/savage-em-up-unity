using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : FastMonoBehaviour {

    public string nom;
    [TextArea]
    public string description;
    public Damage damage;
    public float maxRange = 10;
    public float speed = 20;
    public float cooldown = 0.0f;
    public WeaponShot shot;
    public GameObject bulletPrefab;

    protected Character owner;
    protected Timer timer;

    public void Initialize(Character owner) {
        this.owner = owner;
        timer = new Timer(cooldown, setOver: true);
    }

    public bool TryUse(Vector3 direction) {
        if(!timer.IsOver()) {
            return false;
        }
        Use(direction);
        timer.Reset();
        return true;
    }

    protected void Use(Vector3 direction) {
        List<Vector3> directions = shot.GetShotsDirections(direction);
        foreach (Vector3 bulletDirection in directions) {
            FireBullet(bulletDirection);
        }
    }

    protected void FireBullet(Vector3 bulletDirection) {
        Vector3 bulletStartPosition = owner.pos + bulletDirection * 0.25f;
        Bullet bullet = Instantiate(bulletPrefab, bulletStartPosition, Quaternion.identity, parent: gm.map.bulletFolder).GetComponent<Bullet>();
        bullet.Initialize(damage, maxRange, speed, owner.faction, bulletDirection);
    }
}
