using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponShot : MonoBehaviour {

    public int nbBullets = 1;
    public float flatDispersion = 0.0f;
    public float randomDispersion = 0.0f;

    public List<Vector3> GetShotsDirections(Vector3 direction) {
        direction = Vector3.ProjectOnPlane(direction.normalized, Vector3.up);

        List<Vector3> directions = new List<Vector3>();
        for (int i = 0; i < nbBullets; i++) {
            Vector3 newDirection = direction;
            if (flatDispersion != 0.0f) {
                float flatAngle = GetFlatAngle(i);
                newDirection = Quaternion.AngleAxis(flatAngle, Vector3.up) * newDirection;
            }
            if (randomDispersion != 0.0f) {
                float randomAngle = UnityEngine.Random.Range(-randomDispersion, randomDispersion) / 2;
                newDirection = Quaternion.AngleAxis(randomAngle, Vector3.up) * newDirection;
            }
            newDirection.Normalize();
            directions.Add(newDirection);
        }
        return directions;
    }

    protected float GetFlatAngle(int i) {
        if (i == 0) {
            return 0.0f;
        }
        float stepAngle = flatDispersion / (nbBullets - 1);
        float flatAngle = i * stepAngle - flatDispersion / 2;
        return flatAngle;
    }
}
