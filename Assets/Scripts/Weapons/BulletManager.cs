using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BulletManager : MonoBehaviour {

    [HideInInspector]
    public UnityEvent<Bullet> onCreateBullet = new UnityEvent<Bullet>();

    public void Initialize() {
    }
}
