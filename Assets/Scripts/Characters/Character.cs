using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Character : FastMonoBehaviour {

    public Faction faction;
    public Health health;
    public Hitable hitable;
    public Speed speed;
    public Controller controller;
    public CharacterUI characterUi;
    public Weapon weapon;

    [HideInInspector]
    public UnityEvent<Character> onDie = new UnityEvent<Character>();

    public virtual void Initialize() {
        health.Initialize(this);
        hitable.Initialize(gameObject);
        speed.Initialize(this);
        controller.Initialize(this);
        characterUi.Initialize(this);
        weapon.Initialize(this);
    }

    public virtual void Shot(Vector3 direction) {
        weapon.TryUse(direction);
    }

    public virtual void Die() {
        Destroy(gameObject);
        onDie.Invoke(this);
    }
}
