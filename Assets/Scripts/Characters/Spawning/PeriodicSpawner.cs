using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeriodicSpawner : Spawner {

    public float firstOffset = 0.0f;
    public float spawnPeriod = 5f;

    protected Coroutine coroutine;

    public override void Initialize() {
        base.Initialize();
        coroutine = StartCoroutine(CStartPeriodicSpawning());
    }

    protected IEnumerator CStartPeriodicSpawning() {
        yield return new WaitForSeconds(firstOffset);
        while(true) {
            Spawn();
            yield return new WaitForSeconds(spawnPeriod);
        }
    }

    public override void Stop() {
        base.Stop();
        if(coroutine != null) {
            StopCoroutine(coroutine);
        }
    }
}
