using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitialSpawner : Spawner {

    public float delay = 0.0f;

    public override void Initialize() {
        base.Initialize();
        StartCoroutine(CSpawnIn());
    }

    protected IEnumerator CSpawnIn() {
        yield return new WaitForSeconds(delay);
        Spawn();
    }
}
