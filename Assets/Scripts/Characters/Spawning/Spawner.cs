using System.Collections.Generic;
using System;
//using UnityEditor.Experimental.GraphView;
using UnityEngine;

public abstract class Spawner : FastMonoBehaviour
{

    public List<SpawningQuantity> spawningQuantities;

    public virtual void Initialize() {
    }

    public List<Ennemy> Spawn() {
        return gm.ennemyManager.Spawn(spawningQuantities);
    }

    public virtual void Stop() { }

    public List<GameObject> GetPrefabs() {
        List<GameObject> prefabs = new List<GameObject>();
        foreach (SpawningQuantity sq in spawningQuantities) {
            prefabs.AddRange(sq.GetPrefabs());
        }
        return prefabs;
    }
}
