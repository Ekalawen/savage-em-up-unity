using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TrainController : Controller
{
    public int nbTargets = 30;
    public float circleRadius = 15.0f;

    protected List<Vector3> targets;
    protected Vector3 currentTarget;

    public override void Initialize(Character owner) {
        base.Initialize(owner);
        InitTargets();
    }

    protected void InitTargets() {
        targets = new List<Vector3>();
        Vector3 center = gm.map.GetCenter();
        float angleStep = 360 / nbTargets;
        for (int i = 0; i < nbTargets; i++) {
            Vector3 target = center + Quaternion.AngleAxis(angleStep * i, Vector3.up) * (Vector3.forward * circleRadius);
            targets.Add(target);
        }
        currentTarget = targets.First();
    }

    public override void UpdateMove() {
        MoveTo(currentTarget);
        if (IsArrived()) {
            currentTarget = GetNextTarget();
        }
        LookAt(currentTarget);
    }

    protected bool IsArrived() {
        return Vector3.Distance(owner.pos, currentTarget) <= 2.0f;
    }

    protected Vector3 GetNextTarget() {
        int index = targets.IndexOf(currentTarget);
        int nextIndex = (index + 1) % targets.Count;
        return targets[nextIndex];
    }

    protected void LookAt(Vector3 position) {
        Vector3 direction = Vector3.ProjectOnPlane(position - owner.pos, Vector3.up);
        owner.transform.LookAt(owner.pos + direction);
    }
}
