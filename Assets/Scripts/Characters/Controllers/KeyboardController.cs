using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class KeyboardController : Controller {

    protected InputManager input;

    public override void Initialize(Character character) {
        base.Initialize(character);
        input = gm.inputManager;
        owner.onDie.AddListener(SwapCharacterOnDeath);
    }

    public override void UpdateMove()
    {
        Move();

        Shoot();

        TrySwapCharacter();
    }

    protected void TrySwapCharacter() {
        if (!input.SwitchCharacter()) {
            return;
        }
        SwapCharacter();
    }

    private void SwapCharacter() {
        owner.onDie.RemoveListener(SwapCharacterOnDeath);
        SquadCharacter currentCharacter = owner.GetComponent<SquadCharacter>();
        SquadCharacter nextCharacter = gm.squadManager.GetNextCharacterFor(currentCharacter);
        nextCharacter.PossessWith(this);
        currentCharacter.UseBackgroundController();
    }

    protected void SwapCharacterOnDeath(Character deadCharacter) {
        SwapCharacter();
    }

    private void Move()
    {
        Vector3 direction = input.GetMovement();
        MoveIn(direction);
        LookAtShootDirection();
    }

    private void LookAtShootDirection()
    {
        Vector3 shootDirection = input.ShootWorldPosition(gm.cameraManager.camera) - owner.pos;
        shootDirection = Vector3.ProjectOnPlane(shootDirection, Vector3.up);
        owner.transform.LookAt(owner.pos + shootDirection);
    }

    protected void Shoot() {
        if (!input.IsShooting()) {
            return;
        }
        Vector3 shootDirection = input.ShootWorldPosition(gm.cameraManager.camera) - owner.pos;
        owner.Shot(shootDirection);
    }
}
