using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnnemyController : Controller
{

    public enum EnnemyState {
        Move,
        Attack,
    }

    public float attackRangeOffset = 0.5f;

    protected EnnemyState state;

    public override void Initialize(Character owner) {
        base.Initialize(owner);
        state = EnnemyState.Move;
    }

    public override void UpdateMove() {
        SquadCharacter closest = GetClosestSquadCharacter();
        if(closest == null ) {
            return;
        }
        float distance = Vector3.Distance(closest.pos, pos);
        float maxAttackRange = owner.weapon.maxRange;
        float startAttackRange = maxAttackRange - attackRangeOffset;
        if (distance > maxAttackRange) {
            Move(closest);
        } else if (state == EnnemyState.Attack) {
            Attack(closest);
        } else if (distance <= startAttackRange) {
            Attack(closest);
        } else {
            Move(closest);
        }
    }

    protected void Attack(SquadCharacter closest)
    {
        owner.Shot(closest.pos - pos);
        MoveTo(owner.pos); // Apply gravity !
        state = EnnemyState.Attack;
    }

    protected void Move(SquadCharacter closest)
    {
        MoveTo(closest.pos);
        state = EnnemyState.Move;
    }

    protected void Move() {
        throw new NotImplementedException();
    }

    protected SquadCharacter GetClosestSquadCharacter() {
        return gm.squadManager.GetSquadAndTrain().OrderBy(s => Vector3.SqrMagnitude(s.pos - pos)).FirstOrDefault();
    }
}
