using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SquadController : Controller
{

    public float trainCoef = 2.0f;
    public float fleeRange = 7.0f;
    public float attackRangeOffset = 1.5f;

    protected SquadCharacter train;

    override public void Initialize(Character owner) {
        base.Initialize(owner);
        train = gm.squadManager.GetTrain();
    }

    public override void UpdateMove()
    {
        Ennemy target = GetTargetEnnemy();
        if (target == null) {
            MoveAwayFromComradesAndGetCloseToTrain();
            return;
        }
        if (IsInRange(target)) {
            Ennemy closest = GetClosestEnnemy();
            if (IsInFleeRange(closest)) {
                MoveIn(owner.pos - closest.pos); // Move away from the closest ennemy
            } else {
                MoveAwayFromComradesAndGetCloseToTrain();
            }
            owner.Shot(target.pos - owner.pos);
        } else {
            MoveIn(target.pos - owner.pos); // Charge the target
            owner.Shot(target.pos - owner.pos);
        }
        LookAt(target);
    }

    protected void MoveAwayFromComradesAndGetCloseToTrain() {
        List<SquadCharacter> comrades = gm.squadManager.squadCharacters.Where(c => c != owner).ToList();
        Vector3 barycenter = Enumerable.Aggregate(comrades, Vector3.zero, (acc, c) => acc + c.pos) / comrades.Count;
        Vector3 direction = Vector3.ProjectOnPlane(owner.pos - barycenter, Vector3.up).normalized;
        Vector3 trainDirection = Vector3.ProjectOnPlane(train.pos - owner.pos, Vector3.up).normalized;
        Vector3 mixedDirection = (direction + trainDirection).normalized;
        MoveIn(mixedDirection);
    }

    protected bool IsInRange(Ennemy target) {
        float distanceToMe = Vector3.Distance(target.pos, owner.pos);
        return distanceToMe <= owner.weapon.maxRange - attackRangeOffset;
    }

    protected bool IsInFleeRange(Ennemy closest) {
        float distanceToMe = Vector3.Distance(closest.pos, owner.pos);
        return distanceToMe <= fleeRange;
    }

    protected void LookAt(Ennemy closest) {
        Vector3 direction = Vector3.ProjectOnPlane(closest.pos - owner.pos, Vector3.up);
        owner.transform.LookAt(owner.pos + direction);
    }

    protected Ennemy GetTargetEnnemy() {
        return gm.ennemyManager.ennemies.OrderBy(e => ComputeScore(e)).FirstOrDefault();
    }

    protected Ennemy GetClosestEnnemy() {
        return gm.ennemyManager.ennemies.OrderBy(e => Vector3.SqrMagnitude(owner.pos - e.pos)).FirstOrDefault();
    }

    protected float ComputeScore(Ennemy ennemy) {
        float distanceToTrain = Vector3.Distance(ennemy.pos, train.pos);
        float distanceToMe = Vector3.Distance(ennemy.pos, owner.pos);
        float score = distanceToTrain * trainCoef + distanceToMe;
        return score;
    }
}
