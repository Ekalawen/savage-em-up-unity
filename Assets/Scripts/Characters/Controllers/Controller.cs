using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Controller : FastMonoBehaviour {

    [HideInInspector]
    public Character owner;
    protected CharacterController characterController;

    public virtual void Initialize(Character owner) {
        this.owner = owner;
        this.characterController = owner.GetComponent<CharacterController>();
        enabled = true;
    }

    public abstract void UpdateMove();

    public void Update() {
        if(!enabled) {
            return;
        }
        if(gm.IsPaused()) {
            return;
        }
        if(gm.IsGameOver()) {
            return;
        }
        UpdateMove();
    }

    private Vector3 Move(Vector3 move) {
        Vector3 gravityMove = Vector3.down * owner.speed.gravitySpeed * Time.deltaTime;
        characterController.Move(move + gravityMove);
        Vector3 horizontalMove = Vector3.ProjectOnPlane(move, Vector3.up);
        if (horizontalMove != Vector3.zero) {
            owner.transform.LookAt(owner.pos + horizontalMove);
        }
        return move;
    }

    protected Vector3 MoveTo(Vector3 target) {
        Vector3 direction = target - owner.pos;
        direction.y = 0;
        float distance = direction.magnitude;
        direction = Vector3.Normalize(direction);
        Vector3 movement = direction * GetSpeed() * Time.deltaTime;
        movement = Vector3.ClampMagnitude(movement, distance);
        return Move(movement);
    }

    protected Vector3 MoveIn(Vector3 direction) {
        Vector3 target = owner.pos + direction.normalized * GetSpeed();
        return MoveTo(target);
    }

    public float GetSpeed() {
        return owner.speed.speed;
    }
}
