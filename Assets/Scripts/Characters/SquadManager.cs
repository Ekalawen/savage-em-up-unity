using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class SquadManager : FastMonoBehaviour {

    public float spawningRadius = 5.0f;
    public List<GameObject> squadCharactersPrefabs;
    public KeyboardController playerController;
    public GameObject trainPrefab;

    [HideInInspector]
    public List<SquadCharacter> squadCharacters;
    protected int nbSquadCharacters;
    protected SquadCharacter train;

    [HideInInspector]
    public UnityEvent onPossessCharacter = new UnityEvent();

    public void Initialize() {
        SpawnTheTrain();
        SpawnInitialSquad();
        PossessFirstSquadMember();
    }

    protected void SpawnTheTrain() {
        Vector3 spawningPos = gm.map.GetCenter() + Vector3.up * gm.map.spawningHeight;
        SquadCharacter newTrain = Instantiate(trainPrefab, spawningPos, Quaternion.identity).GetComponent<SquadCharacter>();
        newTrain.Initialize();
        newTrain.onDie.AddListener(LoseGameOnTrainDeath);
        train = newTrain;
    }

    protected void LoseGameOnTrainDeath(Character train) {
        gm.SetGameIsOver();
    }

    private void SpawnInitialSquad() {
        squadCharacters = new List<SquadCharacter>();
        nbSquadCharacters = squadCharactersPrefabs.Count;
        for (int i = 0; i < nbSquadCharacters; i++) {
            float spawningAngle = (360 / nbSquadCharacters) * i;
            Vector3 spawningOffset = Quaternion.AngleAxis(spawningAngle, Vector3.up) * (Vector3.forward * spawningRadius);
            Vector3 spawningPos = gm.map.GetCenter() + Vector3.up * gm.map.spawningHeight + spawningOffset;
            SquadCharacter squadCharacter = Instantiate(squadCharactersPrefabs[i], spawningPos, Quaternion.identity).GetComponent<SquadCharacter>();
            squadCharacter.Initialize();
            squadCharacter.onDie.AddListener(Unregister);
            squadCharacter.onDie.AddListener(SetGameOverOnLastCharacterDeath);
            Register(squadCharacter);
        }
    }

    protected void PossessFirstSquadMember() {
        SquadCharacter squadCharacter = squadCharacters.First();
        squadCharacter.PossessWith(playerController);
    }

    protected void SetGameOverOnLastCharacterDeath(Character character) {
        if (squadCharacters.Count == 0) {
            gm.SetGameIsOver();
        }
    }

    protected void Register(SquadCharacter squadCharacter) {
        squadCharacters.Add(squadCharacter);
    }

    protected void Unregister(Character character) {
        SquadCharacter squadCharacter = character.GetComponent<SquadCharacter>();
        squadCharacters.Remove(squadCharacter);
    }

    public SquadCharacter GetNextCharacterFor(SquadCharacter squadCharacter) {
        int index = squadCharacters.IndexOf(squadCharacter);
        int nextIndex = index != -1 ? (index + 1) % squadCharacters.Count : 0;
        return squadCharacters[nextIndex];
    }

    public SquadCharacter GetPossessedCharacter() {
        return playerController.owner.GetComponent<SquadCharacter>();
    }

    public List<SquadCharacter> GetSquadAndTrain() {
        List<SquadCharacter> res = squadCharacters.Select(s => s).ToList();
        res.Add(train);
        return res;
    }

    public SquadCharacter GetTrain() {
        return train;
    }
}
