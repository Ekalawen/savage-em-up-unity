using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquadCharacter : Character {

    public Weapon secondaryWeapon;

    protected Controller backgroundController;

    public override void Initialize() {
        base.Initialize();
        secondaryWeapon.Initialize(this);
    }

    public void SwitchWeapons() {
        Weapon tmpWeapon = weapon;
        weapon = secondaryWeapon;
        secondaryWeapon = tmpWeapon;
    }

    public void PossessWith(Controller newController) {
        backgroundController = controller;
        backgroundController.enabled = false;
        controller = newController;
        controller.Initialize(this);
        gm.squadManager.onPossessCharacter.Invoke();
    }

    public void UseBackgroundController() {
        controller = backgroundController;
        controller.enabled = true;
    }
}
