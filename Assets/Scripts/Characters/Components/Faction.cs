using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Faction : MonoBehaviour
{
    public enum FactionType {
        Squad,
        Enemy
    }

    public FactionType type;
}
