using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage : MonoBehaviour {

    public int quantity = 1;

    [HideInInspector]
    public GameObject owner;

    public void Initialize(GameObject owner) {
        this.owner = owner;
    }
}
