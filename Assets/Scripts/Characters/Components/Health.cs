using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class Health : MonoBehaviour {

    public int initialHealth = 10;

    [HideInInspector]
    public Character owner;
    [HideInInspector]
    public int maxHealth;
    [HideInInspector]
    public int currentHealth;

    [HideInInspector]
    public UnityEvent onLifeChange = new UnityEvent();


    public void Initialize(Character owner) {
        this.owner = owner;
        maxHealth = initialHealth;
        currentHealth = maxHealth;
    }

    public int TakeDamage(Damage damage) {
        Assert.IsTrue(damage.quantity >= 0);
        int damageTaken = Mathf.Min(damage.quantity, currentHealth);
        currentHealth -= damageTaken;
        if (currentHealth <= 0) {
            Die();
        }
        onLifeChange.Invoke();
        return damageTaken;
    }

    public void Die() {
        owner.Die();
    }
}
