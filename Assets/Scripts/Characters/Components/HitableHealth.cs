using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Health))]
public class HitableHealth : Hitable {

    protected Health health;

    public override void Initialize(GameObject owner) {
        base.Initialize(owner);
        health = GetComponent<Health>();
    }

    public override void HitBy(Damage damage) {
        if (damage.owner == owner) {
            return;
        }
        health.TakeDamage(damage);
    }
}
