using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CharacterUI : MonoBehaviour {

    public TMP_Text nameText;
    public Slider healthSlider;

    protected Character owner;

    public void Initialize(Character owner) {
        this.owner = owner;
        nameText.text = owner.name.Replace("(Clone)", "");
        healthSlider.maxValue = owner.health.maxHealth;
        owner.health.onLifeChange.AddListener(UpdateHealth);
        UpdateHealth();
    }

    public void UpdateHealth() {
        healthSlider.value = owner.health.currentHealth;
    }
}
