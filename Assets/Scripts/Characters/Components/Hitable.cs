using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Hitable : MonoBehaviour {

    [HideInInspector]
    public GameObject owner;

    public virtual void Initialize(GameObject owner) {
        this.owner = owner;
    }

    public abstract void HitBy(Damage damage);
}
