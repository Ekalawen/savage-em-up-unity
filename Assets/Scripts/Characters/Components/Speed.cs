using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Speed : MonoBehaviour {

    public float speed = 5;
    public float gravitySpeed = 2;

    [HideInInspector]
    public Character owner;

    public void Initialize(Character owner) {
        this.owner = owner;
    }
}
