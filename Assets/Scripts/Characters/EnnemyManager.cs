using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EnnemyManager : FastMonoBehaviour {

    public int maxEnnemyCount = 100;
    public List<Spawner> spawners;

    [HideInInspector]
    public List<Ennemy> ennemies;

    [HideInInspector]
    public UnityEvent<Ennemy> onEnnemyDie = new UnityEvent<Ennemy>();

    public void Initialize() {
        ennemies = new List<Ennemy>();
        InitializeSpawners();
    }

    protected void InitializeSpawners() {
        foreach(Spawner spawner in spawners) {
            spawner.Initialize();
        }
    }

    protected void Register(Ennemy ennemy) {
        ennemies.Add(ennemy);
    }

    protected void Unregister(Character character) {
        Ennemy ennemy = character.GetComponent<Ennemy>();
        ennemies.Remove(ennemy);
    }

    public List<Ennemy> Spawn(List<SpawningQuantity> spawningQuantities) {
        List<Ennemy> spawnedEnnemies = new List<Ennemy>();
        foreach (SpawningQuantity spawningQuantity in spawningQuantities) {
            spawnedEnnemies.AddRange(Spawn(spawningQuantity));
        }
        return spawnedEnnemies;
    }

    public List<Ennemy> Spawn(SpawningQuantity spawningQuantity) {
        List<Ennemy> spawnedEnnemies = new List<Ennemy>();
        foreach(GameObject ennemyPrefab in spawningQuantity.GetPrefabs()) {
            spawnedEnnemies.Add(Spawn(ennemyPrefab));
        }
        return spawnedEnnemies;
    }

    protected Ennemy Spawn(GameObject ennemyPrefab) {
        if(ennemies.Count >= maxEnnemyCount) {
            return null;
        }
        Ennemy ennemy = Instantiate(ennemyPrefab, GetSpawnPosition(), Quaternion.identity, parent: transform).GetComponent<Ennemy>();
        ennemy.Initialize();
        Register(ennemy);
        ennemy.onDie.AddListener(Unregister);
        ennemy.onDie.AddListener(OnEnnemyDie);
        return ennemy;
    }

    protected void OnEnnemyDie(Character ennemy) {
        onEnnemyDie.Invoke(ennemy.GetComponent<Ennemy>());
    }

    protected Vector3 GetSpawnPosition() {
        return gm.map.GetRandomSpawningPosition();
    }
}
