using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class MapManager : FastMonoBehaviour {

    public float spawningHeight = 5.0f;
    public GameObject floor;

    [HideInInspector]
    public Transform bulletFolder;

    public void Initialize() {
        bulletFolder = new GameObject("Bullets").transform;
        bulletFolder.SetParent(transform);
    }

    public Vector3 GetCenter() {
        return floor.transform.position;
    }

    public Vector3 GetMapSize() {
        return new Vector3(floor.transform.localScale.x, 0, floor.transform.localScale.z);
    }

    public Vector3 GetRandomSpawningPosition() {
        float x = UnityEngine.Random.Range(-GetMapSize().x / 2, GetMapSize().x / 2);
        float y = spawningHeight;
        float z = UnityEngine.Random.Range(-GetMapSize().z / 2, GetMapSize().z / 2);
        return GetCenter() + new Vector3(x, y, z);
    }
}
