using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : FastMonoBehaviour
{

    protected Transform cameraTransform;

    void Start() {
        cameraTransform = Camera.main.transform;
    }

    void Update() {
        //transform.rotation = Quaternion.LookRotation(pos - cameraTransform.position, Vector3.up);
        transform.LookAt(pos + cameraTransform.forward);
    }
}
