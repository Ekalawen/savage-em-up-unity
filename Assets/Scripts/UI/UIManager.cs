using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UIManager : FastMonoBehaviour {

    public TMP_Text timer;
    public TMP_Text killCounter;
    public TMP_Text squadCharacterName;
    public TMP_Text weaponDescription;

    protected int nbKills = 0;

    public void Initialize() {
        nbKills = 0;
        gm.ennemyManager.onEnnemyDie.AddListener(UpdateKillCounter);
        SetKillCounterToNbKills();
        gm.squadManager.onPossessCharacter.AddListener(SetUnitAndWeaponText);
        SetUnitAndWeaponText();
    }

    public void SetUnitAndWeaponText() {
        SquadCharacter character = gm.squadManager.GetPossessedCharacter();
        squadCharacterName.SetText(character.name.Replace("(Clone)", ""));
        weaponDescription.SetText($"{character.weapon.nom}\n{character.weapon.description}");
        LayoutRebuilder.ForceRebuildLayoutImmediate(weaponDescription.rectTransform);
    }

    public void Update() {
        UpdateTimer();
    }

    private void UpdateTimer() {
        if(gm.IsGameOver()) {
            return;
        }
        float time = gm.timerManager.GetRealElapsedTime();
        timer.text = $"{TimerManager.TimerToString(time)}s";
    }

    public void UpdateKillCounter(Ennemy ennemy)
    {
        nbKills++;
        SetKillCounterToNbKills();
    }

    private void SetKillCounterToNbKills()
    {
        killCounter.text = $"{nbKills} Kills";
    }
}
