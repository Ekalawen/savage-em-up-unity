using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static UnityEngine.UI.GridLayoutGroup;

public class InputManager : Singleton<InputManager>
{

    protected PlayerInputAction actions;

    public override void Awake() {
        base.Awake();
        name = "InputManager";
        actions = new PlayerInputAction();
        DontDestroyOnLoad(this);
    }

    public void OnEnable() {
        actions.Enable();
    }

    public void OnDisable() {
        actions.Disable();
    }

    public bool GetRestartGame() {
        return Input.GetKeyDown(KeyCode.R);
    }

    public bool GetPauseGame() {
        return Input.GetKeyDown(KeyCode.P);
    }

    public bool GetQuitGame() {
        return Input.GetKeyDown(KeyCode.Escape);
    }

    public Vector3 GetMovement() {
        Vector2 move = actions.Player.Movement.ReadValue<Vector2>();
        return new Vector3(move.x, 0, move.y);
    }

    public bool IsShooting() {
        return actions.Player.Shoot.IsPressed();
    }

    public Vector2 ShootPosition() {
        return actions.Player.ShootPosition.ReadValue<Vector2>();
    }

    public Vector3 ShootWorldPosition(Camera camera) {
        Vector2 shootPosition = ShootPosition();
        Ray ray = camera.ScreenPointToRay(shootPosition);
        if (!Physics.Raycast(ray, out RaycastHit hitInfo)) {
            return Vector3.zero;
        }
        Vector3 worldPosition = hitInfo.point;
        return worldPosition;
    }

    public bool SwitchCharacter() {
        return actions.Player.SwitchCharacter.triggered;
    }
}
