using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine.VFX;

public class PostProcessManager : FastMonoBehaviour {

    public GameObject bulletHitSquadParticlePrefab;
    public GameObject bulletHitEnnemyParticlePrefab;

    public void Initialize() {
        gm.bulletManager.onCreateBullet.AddListener(OnCreateBullet);
    }

    protected void OnCreateBullet(Bullet bullet) {
        bullet.onHit.AddListener(OnBulletImpact);
    }

    protected void OnBulletImpact(Bullet bullet) {
        VisualEffect particle = null;
        if(bullet.faction.type == Faction.FactionType.Squad) {
            particle = Instantiate(bulletHitSquadParticlePrefab, bullet.transform.position, Quaternion.LookRotation(bullet.direction)).GetComponentInChildren<VisualEffect>();
        } else {
            particle = Instantiate(bulletHitEnnemyParticlePrefab, bullet.transform.position, Quaternion.LookRotation(bullet.direction)).GetComponentInChildren<VisualEffect>();
        }
        Destroy(particle.gameObject, 1.0f);
    }
}
